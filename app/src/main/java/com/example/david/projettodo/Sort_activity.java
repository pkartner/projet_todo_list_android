package com.example.david.projettodo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class Sort_activity extends AppCompatActivity {
    private CheckBox ch1,ch2;
    private ListView lv;
    private SimpleCursorAdapter dataAdapter;
    private boolean sorted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort);
        ch1 = ((CheckBox) findViewById(R.id.checkBox));
        ch2 = ((CheckBox) findViewById(R.id.checkBox2));
        ch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {


                } else {

                }

            }
        });
    }

    private void displayItems() {
        Cursor cursor;



        cursor = TodoBase.fetchAllItems(this, sorted);
        dataAdapter = new SimpleCursorAdapter(this, R.layout.row, cursor,
                new String[]{TodoBase.KEY_LABEL},
                new int[]{R.id.label},
                0);

        lv.setAdapter(dataAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sort_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
